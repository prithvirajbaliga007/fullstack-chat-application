import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import ProtectedRoute from "./components/ProtectedRoute";
// import Header from './components/Header/Header';

import Login from './pages/Login/Login';
import Loader from './components/common/Loader/Loader';
import socket from './common/helper/socket';
const LazyRegistration = React.lazy(() => import('./pages/Registration/Registration'));
const LazyChatDashboard = React.lazy(() => import('./pages/ChatDashboard/ChatDashboard'))
const LazyProfilePage = React.lazy(() => import('./pages/ProfilePage/ProfilePage'))
const LazyNotFound = React.lazy(() => import('./pages/NotFound/NotFound'))

function App() {

  useEffect(()=> {
    return () => socket.disconnect();
  },[])

  return (
    <>
      {/* <ApiMsg/>*/}
      {/* <Header /> */}
        <React.Suspense fallback={<Loader/>}>
          <BrowserRouter>
            <Switch>
                <Route path="/" component={Login} exact/>
                <Route path="/registration" component={LazyRegistration} />
                <ProtectedRoute exact path="/chat-dashboard" component={LazyChatDashboard} />
                <ProtectedRoute exact path="/profile" component={LazyProfilePage} />
                <Route path="*" component={LazyNotFound} />
            </Switch>
          </BrowserRouter>
        </React.Suspense>
    </>
  );
}

export default App;
