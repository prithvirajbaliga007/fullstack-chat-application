import React , { useEffect, useState, useRef } from 'react';

import { debounce } from 'lodash';
import styles from './UsersChatProfileListing.module.scss';
import { API_URLS, API_METHODS } from '../../../common/constants/api';
import { apiCall } from '../../../common/helper/fetchData';
import userDefaultImage from '../../../assests/images/user-circle-solid.svg';
import auth from '../../../common/helper/auth'
import socket from '../../../common/helper/socket';

const UsersChatProfileListing = ({userChatClick, isConnected}) => {
    const search = useRef();
    const [ usersProfileListing, setUsersProfileListing ] = useState([]);
    const [ isSearch, setIsSearch ] = useState(false);
    const [ selectedUser, setSelectedUser ] = useState('');
    let listing = useRef(usersProfileListing);
    let selectedUserData = useRef(null);

    useEffect(() => {
        getUsersList();
    }, [])

    useEffect(() => {
        socket.on('userIsOnline', (data) => {
            setUserOnlineOrOffline(data, true)
        });
        socket.on('UserWentOffline', data => {
            setUserOnlineOrOffline(data, false)
        });
    }, [socket])

    const setUserOnlineOrOffline = (data, status) => {
        for (let i = 0; i < listing.length; i++) {
            if(data.userId === listing[i]._id) {
                listing[i].online = status;
                setUsersProfileListing([...listing]);
                break;
            }
        }
        userChatClick({ ...selectedUserData.current, online: status})
    }

    useEffect(() => {
        if(isConnected) {
            getUsersList();
        }
    }, [isConnected])

    const getUsersList = () => {
        apiCall(`${API_URLS.conversations}/${auth.getUserDetails()._id}`, API_METHODS.get)
        .then((response) => {
            setUsersProfileListing(response.data)
            listing = response.data;
        })
    }

    const onUserSearchChange = debounce((event) => {
        if(event.target.value) {
            apiCall(API_URLS.searchUser , API_METHODS.get, null, { 
                params: { searchText: event.target.value } 
            })
            .then((response) => {
                if(response) {
                    setUsersProfileListing(response.data);
                    listing = response.data;
                }
            })
        } else {
            setUsersProfileListing([]);
        }
    }, 1000)

    const onCloseSearchList = () => {
        search.current.value = '';
        getUsersList();
        setIsSearch(false);
    }

    const onSearchInputFocus = () => {
        setUsersProfileListing([]);
        setIsSearch(true);
    }

    const onClickUserProfile = (data) => {
        if(data.conversationId) {
            setSelectedUser(data._id);
            selectedUserData.current = data;
            userChatClick(data)
        } else {
            apiCall(`${API_URLS.getUserConversationId}/${data._id}`, API_METHODS.get)
            .then((response) => {
                if(response.data.length) {
                    userChatClick({ ...data, conversationId: response.data[0]._id});
                    selectedUserData.current = { ...data, conversationId: response.data[0]._id };
                    setSelectedUser(data._id);
                    onCloseSearchList();
                } else {
                    userChatClick({ ...data, conversationId: ''});
                    selectedUserData.current = { ...data, conversationId: ''};
                    setSelectedUser(data._id);
                    onCloseSearchList();
                }
            })
        }
    }

    const listItems = usersProfileListing.map((data) => {
        return (<li key={data._id} >
            <button onClick={() => onClickUserProfile(data)} className={`${(data._id === selectedUser) ? `${styles.selectedUser}` : ''}`}>
                <img src={data.profilePicture ? data.profilePicture : userDefaultImage }/>
                <p>{data.name}</p>
                <div className={`${styles.offlineIndicator} ${data.online ? styles.onlineIndicator : ''}`}></div>
            </button>
        </li>)
    });

    return (
        <>
            <div className={styles.searchContainer}>
                <input ref={search} type="text" onFocus={onSearchInputFocus} placeholder="Search" onChange={onUserSearchChange}/>
                { isSearch && <i className="fas fa-times" onClick={onCloseSearchList}></i> }
            </div>
            <ul className={styles.userChatProfileListing}>
                    { usersProfileListing.length ? listItems : null }
            </ul>
        </>
    );
  }
  
  export default UsersChatProfileListing;
  