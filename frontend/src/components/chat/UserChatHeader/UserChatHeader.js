import React, { useEffect } from 'react';

import styles from './UserChatHeader.module.scss';
import userDefaultImage from '../../../assests/images/user-circle-solid.svg';

const UserChatHeader = ({ currentUser }) => {
    
    // useEffect(() => {
    //     console.log(currentUser);
    // }, [currentUser])

    return (
        <div className={styles.userChattingContainer}>
            <div className={styles.userChattingHeader}>
                <div>
                    <div className={styles.userInfo}>
                        <img src={ (currentUser && currentUser.profilePicture) ? currentUser.profilePicture : userDefaultImage}/>
                        <p>{currentUser ? currentUser.name : ''}</p>
                        <div className={`${styles.offlineIndicator} ${currentUser.online ? styles.onlineIndicator : ''}`}></div>
                    </div>
                </div>
                <div>
                    <button>
                        <i className="fas fa-phone-alt"></i>
                        <p>Start a call</p>
                    </button>
                </div>
            </div>
        </div>
    );
  }
  
  export default UserChatHeader;
  