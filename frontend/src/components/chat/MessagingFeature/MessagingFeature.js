import React, { useState, useEffect, useRef } from 'react';

import styles from './MessagingFeature.module.scss';
import userDefaultImage from '../../../assests/images/user-circle-solid.svg';
import UserChatHeader from '../UserChatHeader/UserChatHeader';
import { API_URLS, API_METHODS } from '../../../common/constants/api';
import { apiCall } from '../../../common/helper/fetchData';
import auth from '../../../common/helper/auth'
import socket from '../../../common/helper/socket';

const MessagingFeature = ({ currentUser, onConnectedToUser }) => {
    const chatListing = useRef();
    const [ typeMsg, setTypeMsg ] = useState('');
    const [ messages, setMessages ] = useState([]);
    const [ senderId, setSenderId ] = useState(null);
    const [ conversationId, setConversationId ] = useState('');
    const [ showConnect, setShowConnect ] = useState(false);

    useEffect(() => {
        setSenderId(auth.getUserDetails()._id)
        if(currentUser.conversationId) {
            getConversationMessages(currentUser.conversationId, false);
        } else {
            setConversationId('');
            setShowConnect(true);
        }
    }, [currentUser])

    useEffect(() => {
        socket.on('getMessage', ({ receiverId }) => {
            if(receiverId === auth.getUserDetails()._id) {
                getConversationMessages(currentUser.conversationId, false);
            }
        })  
    },[socket])

    const getConversationMessages = (coversationId, showConnectStatus) => {
        setConversationId(coversationId);
        setShowConnect(showConnectStatus);
        apiCall(`${API_URLS.messages}/${coversationId}`, API_METHODS.get)
        .then((response) => {
            setMessages(response.data)
            if(chatListing.current) {
                setTimeout(() => {
                    chatListing.current.scrollTo(0,chatListing.current.scrollHeight);
                });
            }
        })
    }

    const sendMessage = (event) => {
        event.preventDefault();
        if(typeMsg) {
            apiCall(API_URLS.messages, API_METHODS.post, { 
                conversationId: conversationId,
                sender: senderId,
                text: typeMsg
            })
            .then((response) => {
                setMessages([ ...messages, response.data]);
                chatListing.current.scrollIntoView({ behavior: "smooth" });
                socket.emit('sendMessage', { receiverId: currentUser._id, conversationId });
                setTypeMsg('');
                if(chatListing.current) {
                    chatListing.current.scrollTo(0,chatListing.current.scrollHeight);
                }
            })
        }
    }

    const onConnect = () => {
        apiCall(API_URLS.conversations, API_METHODS.post, { 
            senderId: senderId,
            receiverId: currentUser._id,
        })
        .then((response) => {
            if(response.data) {
                setConversationId(response.data._id);
                setShowConnect(false);
                onConnectedToUser(response.data._id);
                setMessages([]);
            }
        })
        .catch((error) => {
            const { message, code } = error.response.data.meta;
            if(code === 403) {
                getConversationMessages(message, false)
            }
        })
    }

    const displayMessages = messages.map((e) => {
        return (<li key={e._id} className={(senderId === e.sender) ? styles.displayMsgRightSide : ''}>
            <div>
                <img src={ currentUser.profilePicture ? currentUser.profilePicture : userDefaultImage}/>
            </div>
            <div>
                <span>{ (senderId === e.sender) ? 'You' : currentUser.name}, {e.createdAt}</span>
                <p>{e.text}</p>
            </div>
        </li>)
    });

    return (
        <>
            <UserChatHeader currentUser={currentUser}/>
            {   conversationId ?
                <div className={styles.messagingContainer}>
                    <div className={styles.messages}>
                        { 
                            messages.length ?
                            <ul ref={chatListing}>
                                { displayMessages }
                            </ul> : 
                            null
                        }
                    </div>
                    <form className={styles.messagingActions}>
                        <input 
                            value={typeMsg}
                            type="text" 
                            placeholder="Type a message..." 
                            onChange={(e) => setTypeMsg(e.target.value)}/>
                        <button type="submit" onClick={sendMessage}>
                            <p>send</p>
                            <i className="fas fa-chevron-circle-right"></i>
                        </button>
                    </form>
                </div> :null
            }
            {   showConnect ?
                <div className={styles.connectWithUserContainer}>
                    <div className={styles.connectWithUserWrapper}>
                        <img src={ (currentUser.profilePicture) ? currentUser.profilePicture : userDefaultImage}/>
                        <p>{ currentUser.name }</p>
                        <button onClick={onConnect}>Connect</button>
                    </div>
                </div> : null
            }
        </>
    );
  }
  
  export default MessagingFeature;
  