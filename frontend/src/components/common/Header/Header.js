import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import styles from './header.module.scss';
import Auth from "../../../common/helper/auth";
import ConfirmationPopUp from '../ConfirmationPopUp/ConfirmationPopUp';
import Modal from '../Modal/Modal';
import userDefaultImage from '../../../assests/images/user-circle-solid-black.svg';
import socket from '../../../common/helper/socket';
import auth from '../../../common/helper/auth'

const Header = () => {
    const [ isLogout, setIsLogout] = useState(false);
    const logout = (response) => {
        if(response) {
            socket.emit('userIsOffline', { userId: auth.getUserDetails()._id });
            setTimeout(() => {
                socket.disconnect();
                Auth.logout(() => {
                    window.location = '/';
                })
            }, 500);
        }
        setIsLogout(false);
    }

    return (
        <div className={styles.HeaderContainer}>
            <span>
                <i className="far fa-comments"></i>
                LET'S CHAT
            </span>
            <div className={styles.btnsWrapper}>
                <Link to="/profile">
                <img src={userDefaultImage}/>
                </Link>
                <button onClick={() => setIsLogout(true)} title="logout">
                    <p>Logout</p>
                    <i className="fas fa-sign-out-alt"></i>
                </button>
            </div>
            { isLogout && <Modal>
                <ConfirmationPopUp 
                    question="Are you sure, you want to logout?" 
                    onConfirm={logout}/>
            </Modal> }
        </div>
    );
  }
  
  export default Header;
  