import React, { useEffect, useState } from 'react';
import styles from './toastMsg.module.scss';

const ToastMsg = ({ apiStatus }) => {
    const initialStatus = { message: '', isError: false }
    const [status, setStatus] = useState(initialStatus);

    useEffect(() => {
      if(apiStatus) {
        setStatus({ ...apiStatus })
        setTimeout(() => {
          setStatus(initialStatus);
          // resetMsg();
        }, 3000);
      }
    }, [apiStatus])
    return (
      <>
      { (status.message)  && 
        <div className={`${styles.msgWrapper} ${status.isError ? styles.error : ''}`}>
          {status.message}
        </div> 
      }   
      </>
    )
  }
  
export default ToastMsg;
  