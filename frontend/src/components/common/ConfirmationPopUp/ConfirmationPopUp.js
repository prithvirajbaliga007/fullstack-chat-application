import React from 'react';

import styles from './ConfirmationPopUp.module.scss';

const ConfirmationPopUp = ({ question, onConfirm }) => {
    return (
    <div className={styles.confirmationWrapper}>
        <p>{ question }</p>
        <div className={styles.buttonWrapper}>
            <button className={`todo-btn ${styles.danger}`} onClick={() => onConfirm(true)}>Yes</button>
            <button className="todo-btn" onClick={() => onConfirm(false)}>No</button>
        </div>
    </div>);
  }
  
  export default ConfirmationPopUp;
  