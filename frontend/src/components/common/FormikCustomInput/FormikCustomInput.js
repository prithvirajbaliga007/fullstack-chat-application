import React from 'react';
import { Field, ErrorMessage } from 'formik';

import styles from './formikCustomInput.module.scss';
import InputError from "../InputError/InputError";

const FormikCustomInput = (props) => {
    return (
        <div className={styles.formControl}>
            <Field {...props}/>
            <ErrorMessage name={props.name} component={InputError}/>
        </div>
      )
  }
  
  export default FormikCustomInput;
  