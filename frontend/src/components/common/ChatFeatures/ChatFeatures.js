import React from 'react'

import styles from './chatFeatures.module.scss';

const ChatFeatures = () => {
    return (
        <ul>
          <li><h2>Chat features</h2></li>
          <li><span className="fas fa-check"></span>Text chat</li>
          <li><span className="fas fa-check"></span><del>Voice chat</del></li>
          <li><span className="fas fa-check"></span><del>Video chat</del></li>
          <li><span className="fas fa-check"></span><del>Emojis</del></li>
          <li><span className="fas fa-check"></span><del>Audio, video, documents file sharing</del></li>
        </ul>
    )
}

export default ChatFeatures
