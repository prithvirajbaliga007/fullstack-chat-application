import React from 'react';

import styles from './Modal.module.scss';

const Modal = (props) => {
    return (
        <div className={styles.modalContainer}>
            <div className={styles.modalWrapper}>
                {props.children}
            </div>
        </div>
    )
  }
  
  export default Modal;
  