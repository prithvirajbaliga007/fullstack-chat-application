import React from 'react';
import styles from './inputError.module.scss';

const InputError = (props) => {
    return <div className={styles.error}>{props.children}</div>
  }
  
  export default InputError;
  