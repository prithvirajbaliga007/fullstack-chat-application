import React, { useState } from 'react';

import styles from './ResetPassword.module.scss';
import FormikCustomInput from "../common/FormikCustomInput/FormikCustomInput";
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { API_URLS, API_METHODS } from '../../common/constants/api';
import { apiCall } from '../../common/helper/fetchData';
import ToastMsg from "../../components/common/ToastMsg/ToastMsg";

const ResetPassword = ({ resetPasswordStatus }) => {
        const initialValues = {
          currentPassword: '',
          newPassword: '',
        }
        const [apiStatus, setApiStatus] = useState(null);
    
      const validationSchema = Yup.object({
        currentPassword: Yup.string().min(5, 'Minimum 5 characters required').required('Required'),
        newPassword: Yup.string().min(5, 'Minimum 5 characters required').required('Required')
        // .oneOf([Yup.ref('currentPassword'), null], 'Passwords must match')
      })
    
      const onSubmit = (values) => {
        apiCall(API_URLS.resetPassword, API_METHODS.put, values)
        .then((response) => {
            if(response) {
              resetPasswordStatus(false, 'success')
            }
        })
        .catch((error) => {
          setApiStatus({
            message: error.response.data.meta.message,
            isError: true
          })
        })
      }
      
    return (
      <div className={styles.resetPasswordWrapper}>
            <ToastMsg apiStatus={apiStatus} />
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
              <Form>
                  <FormikCustomInput className="todo-input" type='password' name='currentPassword' placeholder="Current password"/>
                  <FormikCustomInput className="todo-input" type='password' name='newPassword' placeholder="New password"/>
                  <button className="todo-btn" type='submit'>Reset</button>
                  <button className="todo-btn" type="button" onClick={() => resetPasswordStatus(false, 'cancel')}>Cancel</button>
              </Form>
          </Formik>
      </div>
    )
  }
  
  export default ResetPassword;
  