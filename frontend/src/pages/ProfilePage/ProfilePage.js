import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import styles from './ProfilePage.module.scss';
import Header from '../../components/common/Header/Header';
import Modal from '../../components/common/Modal/Modal';
import ResetPassword from '../../components/ResetPassword/ResetPassword';
import ConfirmationPopUp from '../../components/common/ConfirmationPopUp/ConfirmationPopUp';
import userDefaultImage from '../../assests/images/user-circle-solid.svg';
import { API_URLS, API_METHODS } from '../../common/constants/api';
import { apiCall } from '../../common/helper/fetchData';
import Auth from "../../common/helper/auth";
import ToastMsg from "../../components/common/ToastMsg/ToastMsg";

const ProfilePage = (props) => {
    const [userProfileDetails, setUserProfileDetails ] = useState(null);
    const [name, setName ] = useState('');
    const [editName, setEditName ] = useState(false);
    const [isDeleteAccount, setIsDeleteAccount ] = useState(false);
    const [isReset, setIsReset ] = useState(false);
    const [apiStatus, setApiStatus] = useState(null);

    useEffect(() => {
        apiCall(API_URLS.userDetails, API_METHODS.get)
        .then((response) => {
            setUserProfileDetails(response.data)
        })
    }, [])

    const convertImgToBase64AndUpload = () => {
        var file = document.querySelector('#uploadProfileImage').files[0];
        if(file.size <= 80000) {
            const base64 = new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
            });
            base64.then(data => {
                apiCall(API_URLS.uploadProfileImage, API_METHODS.post, { profilePicture: data })
                .then((response) => {
                    setUserProfileDetails(response.data);
                })
            })
        }
      }

    const onDeleteAccount = (response) => {
        if(response) {
            apiCall(API_URLS.deleteUserAccount, API_METHODS.delete)
            .then((response) => {
                if(response) {
                    Auth.logout(() => {
                        window.location = '/';
                    })
                }
            })
        } else {
            setIsDeleteAccount(false)
        }
    }

    const onNameSubmit = (event) => {
        event.preventDefault();
        if(name) {
            apiCall(API_URLS.updateUserName , API_METHODS.put, { name: name })
            .then((response) => {
                if(response) {
                    setUserProfileDetails({ ...userProfileDetails, name })
                    setEditName(false);
                    setName('');
                    setApiStatus({
                        message: 'Your profile name is updated successfully',
                        isError: false
                    })
                }
            })
        } else {
            setEditName(false);
            setName('');
        }
    }

    const clickEditName = () => {
        setEditName(true);
        setName(userProfileDetails.name);
    }

    const resetPasswordStatus = (status, type) => {
        setIsReset(status)
        if(type === 'success') {
            console.log(status, type);
            setApiStatus({
                message: 'Password has been reset successfully',
                isError: false
            })
        }
    }

    return (
        <>
            <Header />
            <div className="custom-container">
            {/* resetMsg={() => console.log('sadddddddd')} */}
            <ToastMsg apiStatus={apiStatus} />
                {   
                    userProfileDetails ?
                    (
                    <div className={styles.profilePageWrapper}>

                        {/* back button  */}
                        <Link to="/chat-dashboard">
                            <i className="fas fa-arrow-left"></i>
                        </Link>

                        {/* profile image  */}
                        <label htmlFor="uploadProfileImage">
                            <input accept=".jpg, .jpeg, .png" id="uploadProfileImage" type="file" onChange={convertImgToBase64AndUpload}/>
                            <img src={ userProfileDetails.profilePicture ? userProfileDetails.profilePicture : userDefaultImage}/>
                        </label>

                        {/* profile name  */}
                        <form>
                        { editName ?
                                (<>
                                    <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>
                                    <button onClick={onNameSubmit}>
                                        <i className="fas fa-check"></i>
                                    </button>
                                </>) :
                                (<>
                                    <p>{userProfileDetails.name}</p>
                                    <i className="fas fa-pen" onClick={clickEditName}></i>
                                </>)
                            }
                        </form>

                        {/* email id */}
                        <p>{userProfileDetails.email}</p>

                        {/* Reset password and delete account  */}
                        <div className={styles.buttonWrapper}>
                            <button className={styles.resetPassord} onClick={() => setIsReset(true)}>
                                Reset password
                            </button>
                            <button onClick={() => setIsDeleteAccount(true)}>
                                Delete account
                            </button>
                        </div>



                    </div>
                    ) 
                    :
                    null
                }
            </div>
            { (isDeleteAccount || isReset) ?  
                <Modal>
                    { isDeleteAccount && <ConfirmationPopUp 
                        question="All the conversation data will be delete for ever. Are you sure you want to delete you account?" 
                        onConfirm={onDeleteAccount}/> }
                    { isReset && <ResetPassword resetPasswordStatus={resetPasswordStatus}/> }
                </Modal> : null
            }
        </>
    );
  }
  
  export default ProfilePage;
  