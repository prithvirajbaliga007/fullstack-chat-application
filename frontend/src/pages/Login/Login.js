import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';

import ChatFeatures from "../../components/common/ChatFeatures/ChatFeatures";
import Auth from "../../common/helper/auth";
import FormikCustomInput from "../../components/common/FormikCustomInput/FormikCustomInput";
import ToastMsg from "../../components/common/ToastMsg/ToastMsg";
import styles from '../../styles/login.module.scss';
import { API_URLS, API_METHODS } from '../../common/constants/api';
import { apiCall } from '../../common/helper/fetchData';

const Login = (props) => {

  const [apiStatus, setApiStatus] = useState(null);

  useEffect(() => {
    if(Auth.isAuthenticated()) {
      Auth.login(() => {
        props.history.push('/chat-dashboard');
      })
    }
  },[]);

  const initialValues = {
    email: '',
    password: '',
  }

  const validationSchema = Yup.object({
    email: Yup.string().email('Invalid email format').required('Required'),
    password: Yup.string().min(5, 'Minimum 5 characters required').required('Required'),
  })

  const onSubmit = (values) => {
    apiCall(API_URLS.signIn, API_METHODS.post, { email: values.email, password: values.password })
    .then((response) => {
      Auth.login(() => {
        props.history.push('/chat-dashboard');
      }, response.data)
    })
    .catch((error) => {
      setApiStatus({
        message: error.response.data.meta.message,
        isError: true
      })
    })
  }

    return (
      <div className="custom-container login-registration-container">
        <ToastMsg apiStatus={apiStatus}/>
        <div className={styles.loginFormWrapper}>
          <ChatFeatures/>
          <div className={styles.loginFormContainer}>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
              <Form>
                <FormikCustomInput className="todo-input" type='email' name='email'  placeholder="E-mail"/>
                <FormikCustomInput className="todo-input" type='password' name='password' placeholder="Password"/>
                <button className="todo-btn" type='submit'>LOGIN</button>
              </Form>
            </Formik>
            <p>Don't have an account?<Link to="/registration"> Sign up</Link></p>
          </div>
        </div>
      </div>
    );
  }
  
  export default Login;
  