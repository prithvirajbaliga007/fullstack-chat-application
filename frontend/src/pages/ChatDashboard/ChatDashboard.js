import React, { useState, useEffect } from 'react'

import styles from './ChatDashboard.module.scss';

import UsersChatProfileListing from '../../components/chat/UsersChatProfileListing/UsersChatProfileListing';
import MessagingFeature from '../../components/chat/MessagingFeature/MessagingFeature';
import Header from '../../components/common/Header/Header';
import auth from '../../common/helper/auth';
import socket from '../../common/helper/socket';

const ChatDashboard = () => {
    const [ currentUser, setCurrentUser ] = useState(null);
    const [ connectUser, setConnectUser ] = useState('');

    useEffect(() => {
        socket.emit('userHasLoggedInToApplication', { userId: auth.getUserDetails()._id });
    }, [socket])

    const onClickUserChat = (data) => {
        setCurrentUser(data)
    }

    const connectedToUser = (data) => {
        setConnectUser(data)
    }
    
    return (
        <>
            <Header />
            <div className={styles.chatDashboard}>
                <div className={styles.chatListing}>
                    <UsersChatProfileListing userChatClick={onClickUserChat} isConnected={connectUser}/>
                </div>
                <div className={styles.chatSpace}>
                    {  currentUser ? <MessagingFeature 
                                        currentUser={currentUser} 
                                        onConnectedToUser={connectedToUser}/> : null }
                </div>
            </div>
        </>

    )
}

export default ChatDashboard
