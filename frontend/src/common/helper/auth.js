import { apiCall } from '../../common/helper/fetchData';
import { API_URLS, API_METHODS } from '../../common/constants/api';

class Auth {
    login = (cb, data) => {
      if(data) {
        localStorage.setItem('auth', data.authToken);
        delete data.authToken
        localStorage.setItem('userDetails', JSON.stringify(data));
      }
      cb();
    }
  
    logout = (cb) => {
      localStorage.removeItem('auth');
      localStorage.removeItem('userDetails');
      cb();
    }
  
    isAuthenticated = () => {
      return localStorage.getItem('auth') ? true : false;
    }

    getAuthToken = () => {
      return localStorage.getItem('auth');
    }

    getUserDetails = () => {
      const userDeatils = JSON.parse(localStorage.getItem('userDetails'));
      if(userDeatils) {
        return userDeatils;
      } else {
        apiCall(API_URLS.userDetails, API_METHODS.get)
        .then((response) => {
          localStorage.setItem('userDetails', JSON.stringify(response.data));
          return response.data;
        })
      }
    }
}
  
export default new Auth();
  