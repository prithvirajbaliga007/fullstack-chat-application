import io from 'socket.io-client';

const socket = io('https://chat-mern-application.herokuapp.com');
// const socket = io('http://localhost:3200');

export default socket;