import axios from 'axios';

import auth from "./auth";
import { API_METHODS } from '../constants/api';

export const apiCall = (url, httpMethod, body = null, optionsPayload = null) => {
    const API = axios.create({ 
        baseURL: `${process.env.REACT_APP_API_BASE_URL}`
    });

    API.interceptors.request.use((req) => {
        const token = auth.getAuthToken();
        if (token) req.headers['x-auth-token'] = token;
        return req;
    });
    
    let options = {};
    if(optionsPayload) options = { ...optionsPayload };
    let response = null;
    switch (httpMethod) {
        case API_METHODS.get:
            response = API.get(url, options)
            break;
        case API_METHODS.post:
            response = API.post(url, body, options)
            break;  
        case API_METHODS.put:
            response = API.put(url, body, options)
            break; 
        case API_METHODS.delete:
            response = API.delete(url, options)
            break;
        default:
            break;
    }
    return response;
}