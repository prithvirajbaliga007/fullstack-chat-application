export const API_URLS = {
    registration: 'user/registration',
    signIn: 'user/login',
    userDetails: 'user/me',
    uploadProfileImage: 'user/uploadProfilePicture',
    deleteUserAccount: 'user/deleteAccount',
    resetPassword: 'user/resetPassword',
    updateUserName: 'user/updateUserName',
    searchUser: 'user/list',

    conversations: 'conversations',
    messages: 'messages',
    getUserConversationId: 'conversations/getUserConversationId'
}

export const API_METHODS = {
    get: 'GET',
    post: 'POST',
    put: 'PUT',
    delete: 'DELETE'
}