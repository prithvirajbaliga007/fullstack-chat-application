const { OnlineUsers } = require("../models/OnlineUsers");
const { sendUserOnlineOfflineStatus } = require('./user')

const addUserToOnlineUserList = async ({ socket, userId }, callback) => {
    if (!userId) return callback({ error: 'userId requried' });
  
    let onlineUser = new OnlineUsers({ socketId: socket.id, userId: userId });
    await onlineUser.save();
}

const onSendMessage = async ({ receiverId, conversationId, io }, callback) => {
    if (!(receiverId && conversationId)) return callback({ error: 'receiverId and conversationId requried' });
    
    const receiverSocketDetails = await OnlineUsers.find({ userId: receiverId });
    if(receiverSocketDetails.length) {
        io.to(receiverSocketDetails[0].socketId).emit('getMessage', {
            conversationId,
            receiverId
        });
    }
}

const onUserIsOffline = async (userId) => {
    sendUserOnlineOfflineStatus(userId, 'offline');
}

const removeUserFromOnlineUserList = async ({ socket }) => {
    await OnlineUsers.findOneAndDelete({ "socketId": socket.id });
}

exports.addUserToOnlineUserList = addUserToOnlineUserList;
exports.removeUserFromOnlineUserList = removeUserFromOnlineUserList;
exports.onSendMessage = onSendMessage;
exports.onUserIsOffline = onUserIsOffline;

