const Joi = require('joi');
const { Conversation } = require("../models/Conversation");
const { User } = require("../models/user");
const { OnlineUsers } = require("../models/OnlineUsers");

const createNewConversation = async (req, res) => {
  const { error } = validateCreateNewConversation(req.body); 
  if (error) return res.status(400).send({ meta: { message: error.details[0].message, code: 400 }});

  const conversation = await Conversation.find({ members: { $in: [req.body.senderId] } });

  let recordAlreadyExsists = '';
  for (let i = 0; i < conversation.length; i++) {
    if(conversation[i].members.includes(req.body.senderId) && conversation[i].members.includes(req.body.receiverId)) {
      recordAlreadyExsists = conversation[i]._id;
      break;
    }
  }

  if(recordAlreadyExsists) {
    res.status(403).send({ meta: { message: recordAlreadyExsists, code: 403 }});
  } else {
    const newConversation = new Conversation({ members: [req.body.senderId, req.body.receiverId] });
    const savedConversation = await newConversation.save();
    res.status(200).send(savedConversation);
  }
}

const validateCreateNewConversation = (user) => {
  const schema = Joi.object({
      senderId: Joi.string().required(),
      receiverId: Joi.string().required(),
  });
  return schema.validate(user);
}

const getConversationListOfUsers = async (req, res) => {
      const { userId } = req.params; 
      if (!userId) return res.status(400).send({ meta: { message: 'User id required', code: 400 }});

      const conversation = await Conversation.find({ members: { $in: [userId] } });
      const userIdList = [];
      let conversationIds = [];
      conversation.forEach(element => {
        const receiverId = element.members.find((m) => m !== userId);
        if(receiverId) {
          userIdList.push(receiverId);
          conversationIds.push(element._id);
        }
      });

      const userConversationList = await User.find({ _id: { $in: userIdList } }).select('-password');
      let list = [];
      for (let i = 0; i < userConversationList.length; i++) {
        list.push({
          _id: userConversationList[i]._id,
          name: userConversationList[i].name,
          email: userConversationList[i].email,
          profilePicture: userConversationList[i].profilePicture,
          conversationId: conversationIds[i]
        })
      }

      const OnlineUsersList = await OnlineUsers.find({ userId: { $in: userIdList, $ne: req.user._id } });
      for (let i = 0; i < list.length; i++) {
        if(OnlineUsersList.some(e => e.userId === list[i]._id.toString())) {
          list[i].online = true;
        } else {
          list[i].online = false;
        }
      }

      res.status(200).send(list);
}

const getUserConversationId = async (req, res) => {
  const { receiverId } = req.params; 
  if (!receiverId) return res.status(400).send({ meta: { message: 'receiver id required', code: 400 }});

  let conversation = await Conversation.find({ $or: [ 
      { members: [ req.user._id, receiverId] }, 
      { members: [receiverId, req.user._id] } 
    ]
  });
  
  res.status(200).send(conversation);
}

exports.createNewConversation = createNewConversation;
exports.getConversationListOfUsers = getConversationListOfUsers;
exports.getUserConversationId = getUserConversationId;