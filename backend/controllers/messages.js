const Joi = require('joi');
const { Message } = require("../models/Message");

const addANewMessage = async (req, res) => {
    const { error } = validateAddANewMessage(req.body); 
    if (error) return res.status(400).send({ meta: { message: error.details[0].message, code: 400 }});

    const newMessage = new Message(req.body);
    const savedMessage = await newMessage.save();
    res.status(200).json(savedMessage);
}

const validateAddANewMessage = (user) => {
  const schema = Joi.object({
      conversationId: Joi.string().required(),
      sender: Joi.string().required(),
      text: Joi.string().required(),
  });
  return schema.validate(user);
}

const getConversationMessage = async (req, res) => {
    const { conversationId } = req.params; 
    if (!conversationId) return res.status(400).send({ meta: { message: 'conversation id required', code: 400 }});

    const messages = await Message.find({ conversationId: conversationId });
    res.status(200).json(messages);
}

exports.addANewMessage = addANewMessage;
exports.getConversationMessage = getConversationMessage;
