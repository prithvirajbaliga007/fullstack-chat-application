const Joi = require('joi');
const bcrypt = require('bcrypt');
const { User } = require('../models/user');
const { OnlineUsers } = require("../models/OnlineUsers");
const { Conversation } = require("../models/Conversation");

const searchUserByUserName = async (req, res) => {
    const { searchText } = req.query; 
    if (!searchText) return res.status(400).send({ meta: { message: 'Search text by username required', code: 400 }});
  
    // const $regex = escapeStringRegexp(searchText);
    const user = await User.find({ name: { "$regex": searchText, "$options": "i" }, _id: { $ne: req.user._id } }).select('-password');;
    const userIdList = [];
    user.forEach(element => {
        userIdList.push(element._id.toString());
    });

    const OnlineUsersList = await OnlineUsers.find({ userId: { $in: userIdList, $ne: req.user._id } });
    let listing = [];
    for (let i = 0; i<user.length; i++) {
      if(OnlineUsersList.some(e => e.userId === user[i]._id.toString())) {
        listing.push({
            _id: user[i]._id.toString(),
            profilePicture: user[i].profilePicture,
            name: user[i].name,
            email: user[i].email,
            createdAt: user[i].createdAt,
            updatedAt: user[i].updatedAt,
            online: true
          })
      } else {
        listing.push({
            _id: user[i]._id.toString(),
            profilePicture: user[i].profilePicture,
            name: user[i].name,
            email: user[i].email,
            createdAt: user[i].createdAt,
            updatedAt: user[i].updatedAt,
            online: false
          })
      }
    }

    res.status(200).send(listing);
}

const getLoggedInUserDetails = async (req, res) => {
    const user = await User.findById(req.user._id).select('-password');
    res.send({ 
      _id: user._id,
      name: user.name,
      email: user.email,
      profilePicture: user.profilePicture
    })
}

const registration = async (req, res) => {
    const { error } = validateUser(req.body); 
    if (error) return res.status(400).send({ meta: { message: error.details[0].message, code: 400 }});
  
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(400).send({ meta: { message: 'User already registered', code: 400 }});
  
    user = new User({ profilePicture: '', name: req.body.name, email: req.body.email, password: req.body.password});
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
  
    const token = user.generateAuthToken();
    res.status(200).send({
      _id: user._id,
      name: user.name,
      email: user.email,
      authToken: token,
      profilePicture: user.profilePicture
    });
}

const validateUser = (user) => {
    const schema = Joi.object({
        profilePicture: Joi.string(),
        name: Joi.string().min(1).max(50).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    });

    return schema.validate(user);
}

const login = async (req, res) => {
    const { error } = validateLogin(req.body); 
    if (error) return res.status(400).send({ meta: { message: error.details[0].message, code: 400 }});
  
    let user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send({ meta: { message: 'Please enter a valid email address', code: 400 }});
  
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).send({ meta: { message: 'Please enter a valid password', code: 400 }});

    sendUserOnlineOfflineStatus(user._id, 'online');
    
    const token = user.generateAuthToken();

    res.send({ 
        _id: user._id,
        profilePicture: user.profilePicture,
        name: user.name,
        email: user.email, 
        authToken: token 
    });


}

const sendUserOnlineOfflineStatus = async (userID, status) => {
    const userId = userID.toString();
    const conversation = await Conversation.find({ members: { $in: [userId] } });
    const receiverIdList = [];
    conversation.forEach(element => {
      const receiverId = element.members.find((m) => m !== userId);
      if(receiverId) {
        receiverIdList.push(receiverId);
      }
    });


    const OnlineUsersList = await OnlineUsers.find({ userId: { $in: receiverIdList } });
    OnlineUsersList.forEach(element => {
        if(status === 'online') {
            io.to(element.socketId).emit('userIsOnline', {
                userId
            })
        } else if(status === 'offline') {
            io.to(element.socketId).emit('UserWentOffline', {
                userId
            })
        }
    });
}

const validateLogin = (req) => {
    const schema = Joi.object({
      email: Joi.string().min(5).max(255).required().email(),
      password: Joi.string().min(5).max(255).required()
    });
    return schema.validate(req);
}

const uploadProfilePicture = async (req, res) => {
    const { profilePicture } = req.body;
    if (!profilePicture) return res.status(400).send({ meta: { message: 'Upload an image', code: 400 }});
    const user = await User.findOneAndUpdate({ _id: req.user._id }, { profilePicture: profilePicture }).select('-password');
    res.send(user);
}

const deleteAccount = async (req, res) => {
    await User.findByIdAndDelete(req.user._id)
    res.status(204).send();
}

const resetPassword = async (req, res) => {
    const { error } = validateResetPassword(req.body); 
    if (error) return res.status(400).send({ meta: { message: error.details[0].message, code: 400 }});
  
    let user = await User.findOne({ _id: req.user._id });
    const validPassword = await bcrypt.compare(req.body.currentPassword, user.password);
    if (!validPassword) return res.status(400).send({ meta: { message: 'Enter a valid current password', code: 400 }});
  
    const salt = await bcrypt.genSalt(10);
    const newPassword = await bcrypt.hash(req.body.newPassword, salt);
    const status = await User.findOneAndUpdate({ _id: req.user._id }, { password: newPassword })
    res.status(200).send();
}

const validateResetPassword = (req) => {
    const schema = Joi.object({
      currentPassword: Joi.string().min(5).max(255).required(),
      newPassword: Joi.string().min(5).max(255).required()
    });
    return schema.validate(req);
}  

const updateUserName = async (req, res) => {
    const { error } = validateUpdateName(req.body); 
    if (error) return res.status(400).send({ meta: { message: error.details[0].message, code: 400 }});
    
    await User.findOneAndUpdate({ _id: req.user._id }, { name: req.body.name });
    res.status(200).send();
}

const validateUpdateName = (req) => {
    const schema = Joi.object({
      name: Joi.string().min(5).max(255).required(),
    });
    return schema.validate(req);
}

exports.getLoggedInUserDetails = getLoggedInUserDetails;
exports.registration = registration;
exports.login = login;
exports.uploadProfilePicture = uploadProfilePicture;
exports.deleteAccount = deleteAccount;
exports.resetPassword = resetPassword;
exports.updateUserName = updateUserName;
exports.searchUserByUserName = searchUserByUserName;
exports.sendUserOnlineOfflineStatus = sendUserOnlineOfflineStatus;
