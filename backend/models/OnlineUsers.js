const mongoose = require("mongoose");

const OnlineUsersSchema = new mongoose.Schema(
  {
    socketId: {
        type: String,
    },
    userId: {
      type: String,
    }
  }
);

const OnlineUsers = mongoose.model("OnlineUsers", OnlineUsersSchema);

exports.OnlineUsers = OnlineUsers; 
