const express = require('express');
const cors = require('cors')
const error = require('../middleware/error');
const serverRunning = require('../routes/serverRunning');
const user = require('../routes/user');
const messageRoute = require('../routes/messages');
const conversationRoute = require('../routes/conversations');


module.exports = function(app) {
  app.use(cors());
  app.use(express.json());
  app.use('/', serverRunning);
  app.use('/api/user', user);
  app.use("/api/conversations", conversationRoute);
  app.use("/api/messages", messageRoute);
  app.use(error);
}