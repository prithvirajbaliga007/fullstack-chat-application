const winston = require('winston');
const mongoose = require('mongoose');
const config = require('config');

module.exports = function() {
  const db = config.get('db');
  mongoose.connect(db).then(() => {
    const message = `Connected to MongoDB`;
    winston.info(message)
    console.log(message);
  });
}