const { addUserToOnlineUserList, removeUserFromOnlineUserList, 
        onSendMessage, onUserIsOffline } = require('../controllers/socket')
const { Server } = require("socket.io");

module.exports = (httpServer) => {
        const io = new Server(httpServer, {
            cors: {
                origin: ["http://localhost:3000", "https://lets-chat-application.netlify.app"]
            }
        });
        global.io = io;
        io.on("connection", (socket) => {
            socket.on('userHasLoggedInToApplication', 
                ({ userId }, callback) => addUserToOnlineUserList({ socket, userId }, callback));
        
            socket.on('sendMessage',
                (data, callback) => onSendMessage({ ...data, io }, callback));

            socket.on('userIsOffline',
                ({ userId }) => onUserIsOffline(userId));
        
            socket.on('disconnect',
                () => removeUserFromOnlineUserList({ socket }));
        });
}
