const router = require("express").Router();
const auth = require('../middleware/auth');
const { addANewMessage, getConversationMessage } = require('../controllers/messages')

router.post("/", auth, addANewMessage);
router.get("/:conversationId", auth, getConversationMessage);

module.exports = router;
