const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
  res.send('welcome to the let\'s chat application. The server running...');
});

module.exports = router;