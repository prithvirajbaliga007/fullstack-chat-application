const router = require("express").Router();
const auth = require('../middleware/auth');
const { createNewConversation, getConversationListOfUsers, 
        getUserConversationId } = require('../controllers/conversations')

router.post("/", auth, createNewConversation);
router.get("/:userId", auth, getConversationListOfUsers);
router.get("/getUserConversationId/:receiverId", auth, getUserConversationId);
// router.get("/find/:firstUserId/:secondUserId", getConversationOfTwoUserId);

module.exports = router;
