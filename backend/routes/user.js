const auth = require('../middleware/auth');
const router = require('express').Router();
const { getLoggedInUserDetails, registration, login, uploadProfilePicture, 
        deleteAccount, resetPassword, updateUserName, searchUserByUserName } = require('../controllers/user')


router.get('/me', auth, getLoggedInUserDetails);
router.post('/registration', registration);
router.post('/login', login);
router.post('/uploadProfilePicture', auth, uploadProfilePicture);
router.delete('/deleteAccount', auth, deleteAccount);
router.put('/resetPassword', auth, resetPassword);
router.put('/updateUserName', auth, updateUserName);
router.get('/list', auth, searchUserByUserName);

module.exports = router; 
