const winston = require('winston');
const app = require('express')();
const httpServer = require("http").createServer(app);

require('dotenv').config()
require('./startup/logging')();
require('./startup/routes')(app);
require('./startup/socket')(httpServer);
require('./startup/db')();
require('./startup/config')();
require('./startup/validation')();

const port = process.env.PORT || 3200;
httpServer.listen(port, () => {
    const message = `Listening on port ${port}...`;
    winston.info(message);
    console.log(message);
});
